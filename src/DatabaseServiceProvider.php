<?php

namespace LaravelModularDashboard;

use Doctrine\Common\Annotations\AnnotationRegistry;
use LaravelModularDashboard\Database\IDatabaseConnector;
use LaravelModularDashboard\Database\MySQLQueryDatabase;
use LaravelModularDashboard\Database\PgSQLQueryDatabase;
use LaravelModularDashboard\Database\QueryDatabase;
use LaravelModularDashboard\Exceptions\DatabaseException;
use LaravelModularDashboard\Database\DatabaseConnection;
use LaravelModularDashboard\Database\MySQLDatabase;
use LaravelModularDashboard\Database\PgSQLDatabase;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Illuminate\Foundation\Application as LaravelApplication;
use Laravel\Lumen\Application as LumenApplication;

/** @noinspection PhpUnused */
final class DatabaseServiceProvider extends BaseServiceProvider
{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register(): void
    {
        /** @noinspection PhpDeprecationInspection */
        AnnotationRegistry::registerLoader('class_exists');

        $this->app->singleton(DatabaseConnection::class, function () {
            $driver = env("DB_CONNECTION");

            if ($driver === 'mysql')
            {
                return new MySQLDatabase(app(IDatabaseConnector::class));
            }

            if ($driver === 'pgsql')
            {
                return new PgSQLDatabase(app(IDatabaseConnector::class));
            }

            throw new DatabaseException("Database driver not configured ($driver)");
        });

        $this->app->singleton(QueryDatabase::class, function () {
            $driver = env("DB_CONNECTION");

            if ($driver === 'mysql')
            {
                return new MySQLQueryDatabase(app(DatabaseConnection::class));
            }

            if ($driver === 'pgsql')
            {
                return new PgSQLQueryDatabase(app(DatabaseConnection::class));
            }

            throw new DatabaseException("Database driver not supported ($driver)");
        });
    }

    /**
     * Register the config for publishing
     *
     * @noinspection MissingOrEmptyGroupStatementInspection
     * @noinspection PhpStatementHasEmptyBodyInspection
     */
    public function boot(): void
    {
        if ($this->app instanceof LaravelApplication && $this->app->runningInConsole())
        {

        }
        elseif ($this->app instanceof LumenApplication)
        {

        }
    }


}
