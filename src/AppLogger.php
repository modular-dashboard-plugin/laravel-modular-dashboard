<?php

namespace LaravelModularDashboard;


use DateTime;
use Exception;
use Illuminate\Support\Facades\Log;
use Psr\Log\AbstractLogger;
use Psr\Log\LogLevel;


/**
 * Class documentation
 */
final class AppLogger extends AbstractLogger
{

    private static ?AppLogger $instance = null;

    protected array $options = [
        'extension' => 'txt',
        'dateFormat' => 'Y-m-d G:i:s.u',
        'filename' => false,
        'flushFrequency' => false,
        'prefix' => 'log_',
        'logFormat' => false,
        'appendContext' => true,
    ];


    protected string $logLevelThreshold = LogLevel::DEBUG;


    public array $logLevels = [
        LogLevel::EMERGENCY => 0,
        LogLevel::ALERT => 1,
        LogLevel::CRITICAL => 2,
        LogLevel::ERROR => 3,
        LogLevel::WARNING => 4,
        LogLevel::NOTICE => 5,
        LogLevel::INFO => 6,
        LogLevel::DEBUG => 7
    ];


    public static function getInstance(): AppLogger
    {
        if (self::$instance === null)
        {
            self::$instance = new AppLogger();
        }
        return self::$instance;
    }


    private function __construct()
    {

    }


    /**
     * Logs with an arbitrary level.
     *
     * @param string $message
     * @param mixed $level by default info level
     * @param array $context
     */
    public function log($level, $message, array $context = []): void
    {
        if ($this->logLevels[$this->logLevelThreshold] < $this->logLevels[$level])
        {
            return;
        }
        try
        {
            $message = $this->formatMessage($level, $message, $context);
            Log::log($level, $message, $context);
        } /** @noinspection PhpUnusedLocalVariableInspection */
        catch (Exception $e)
        {
            Log::log($level, $message, $context);
        }
    }


    /**
     * Formats the message for logging.
     *
     * @param string $level The Log Level of the message
     * @param string $message The message to log
     * @param array $context The context
     * @return string
     * @throws Exception
     */
    protected function formatMessage(string $level, string $message, array $context): string
    {
        if ($this->options['logFormat'])
        {
            $parts = [
                'date' => $this->getTimestamp(),
                'level' => strtoupper($level),
                'level-padding' => str_repeat(' ', 9 - strlen($level)),
                'priority' => $this->logLevels[$level],
                'message' => $message,
                'context' => json_encode($context, JSON_THROW_ON_ERROR),
            ];
            $message = $this->options['logFormat'];
            foreach ($parts as $part => $value)
            {
                $message = str_replace('{' . $part . '}', $value, $message);
            }

        }
        else
        {
            $message = "[$level]  $message";
        }

        if ($this->options['appendContext'] && !empty($context))
        {
            $message .= PHP_EOL . $this->indent($this->contextToString($context));
        }
        return $message . PHP_EOL;
    }

    /**
     * Gets the correctly formatted Date/Time for the log entry.
     *
     * PHP DateTime is dump, and you have to resort to trickery to get microseconds
     * to work correctly, so here it is.
     *
     * @return string
     * @throws Exception
     */
    private function getTimestamp(): string
    {
        $originalTime = microtime(true);
        $micro = sprintf('%06d', ($originalTime - floor($originalTime)) * 1000000);
        return (new DateTime(date('Y-m-d H:i:s.' . $micro, $originalTime)))->format($this->options['dateFormat']);
    }

    /**
     * Takes the given context and coverts it to a string.
     *
     * @param array $context The Context
     * @return string
     */
    protected function contextToString(array $context): string
    {
        $export = '';
        foreach ($context as $key => $value)
        {
            $export .= "{
            $key}: ";
            $export .= preg_replace([
                '/=>\s+([a-zA-Z])/im',
                '/array\(\s+\)/im',
                '/^ {2}|\G {2}/m'
            ], [
                '=> $1',
                'array()',
                '    '
            ], str_replace('array (', 'array(', var_export($value, true)));
            $export .= PHP_EOL;
        }
        return str_replace(['\\\\', '\\\''], ['\\', '\''], rtrim($export));
    }

    /**
     * Indents the given string with the given indent.
     *
     * @param string $string The string to indent
     * @param string $indent What to use as the indent.
     * @return string
     */
    protected function indent(string $string, string $indent = '    '): string
    {
        return $indent . str_replace("\n", "\n" . $indent, $string);
    }
}
