<?php


namespace LaravelModularDashboard\Route;


use JetBrains\PhpStorm\Pure;

final class RouteType
{
    public const POST = 1;
    public const GET = 2;
    public const PUT = 3;
    public const DELETE = 4;

    private int $type;

    private function __construct(int $type) { $this->type = $type; }

    public function getType(): int
    {
        return $this->type;
    }

    /** @noinspection PhpUnused */
    #[Pure] public static function POST(): RouteType
    {
        return new RouteType(self::POST);
    }

    #[Pure] public static function GET(): RouteType
    {
        return new RouteType(self::GET);
    }

    /** @noinspection PhpUnused */
    #[Pure] public static function PUT(): RouteType
    {
        return new RouteType(self::PUT);
    }

    /** @noinspection PhpUnused */
    #[Pure] public static function DELETE(): RouteType
    {
        return new RouteType(self::DELETE);
    }
}
