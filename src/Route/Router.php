<?php


namespace LaravelModularDashboard\Route;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use LaravelModularDashboard\EndPoints\CommandEndPoint;
use LaravelModularDashboard\Exceptions\AuthorizationException;
use LaravelModularDashboard\Permissions\CustomPermission;
use LaravelModularDashboard\Permissions\FeaturesPermission;
use LaravelModularDashboard\Permissions\PermissionDetails;
use LaravelModularDashboard\Permissions\PermissionGroup;
use LaravelModularDashboard\Permissions\GroupType;
use LaravelModularDashboard\Exceptions\RouterException;
use LaravelModularDashboard\EndPoints\BaseEndPoint;
use LaravelModularDashboard\EndPoints\CSVEndPoint;
use LaravelModularDashboard\EndPoints\QueryEndPoint;
use LaravelModularDashboard\Processors\CommandProcessor;
use LaravelModularDashboard\Processors\CommandRawResponseProcessor;
use LaravelModularDashboard\Processors\CommandResponseProcessor;
use LaravelModularDashboard\Processors\CSVProcessor;
use LaravelModularDashboard\Processors\BaseCommandProcessor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use LaravelModularDashboard\Processors\QueryProcessor;
use LaravelModularDashboard\QueryExtractor\QueryExtractor;
use Symfony\Component\HttpFoundation\BinaryFileResponse;


abstract class Router
{


    private QueryExtractor $extractor;

    /**
     * @param QueryExtractor $extractor
     */
    public function __construct(QueryExtractor $extractor)
    {
        $this->extractor = $extractor;
    }


    abstract protected function getUserPermissions(): array;


    /** @noinspection PhpUnused */
    final public function register(?array $groupSpec = null, BaseEndPoint ...$endpoints): void
    {
        if ($groupSpec === null) {
            foreach ($endpoints as $endPoint) {
                $this->registerEndPoint($endPoint);
            }
        } else {
            Route::group($groupSpec,
                function () use ($endpoints) {
                    foreach ($endpoints as $endPoint) {
                        $this->registerEndPoint($endPoint);
                    }
                });
        }
    }

    /** @noinspection StaticClosureCanBeUsedInspection */
    private function registerEndPoint(BaseEndPoint $endPoint): void
    {
        $router = $this;

        if ($endPoint instanceof CSVEndPoint) {
            Route::get($endPoint->getUrl(), function (Request $request) use ($router, $endPoint) {
                return $router->executeCSVProcessor($request, $endPoint);
            });
        } else if ($endPoint instanceof QueryEndPoint) {
            Route::get($endPoint->getUrl(), function (Request $request) use ($router, $endPoint): Response|JsonResponse {
                return $router->executeQueryProcessor($request, $endPoint);
            });
        } else if ($endPoint instanceof CommandEndPoint) {

            switch ($endPoint->getRouteType()->getType()) {
                case RouteType::POST:
                    Route::post($endPoint->getUrl(), function (Request $request) use ($router, $endPoint): Response|JsonResponse {
                        return $router->executeCommandProcessor($request, $endPoint);
                    });
                    break;
                case RouteType::PUT:
                    Route::put($endPoint->getUrl(), function (Request $request) use ($router, $endPoint): Response|JsonResponse {
                        return $router->executeCommandProcessor($request, $endPoint);
                    });
                    break;
                case RouteType::DELETE:
                    Route::delete($endPoint->getUrl(), function (Request $request) use ($router, $endPoint): Response|JsonResponse {
                        return $router->executeCommandProcessor($request, $endPoint);
                    });
                    break;
            }
        }
    }


    /**
     * @param Request $request
     * @param QueryEndPoint $endPoint
     * @return JsonResponse
     * @throws AuthorizationException
     */
    private function executeQueryProcessor(Request $request, QueryEndPoint $endPoint): JsonResponse
    {
        self::$currentUrl = $request->getPathInfo();

        /** @var QueryProcessor $processor */
        $processor = app($endPoint->getClassName());

        $query = $this->extractor->extract($request->all());

        $processor->setHttpQuery($query);

        if ($this->authorize($processor->authorize($request)) === false) {
            throw new AuthorizationException();
        }

        $response = $processor->execute($request);
        return response()->json($response, 200, ["Content-Type" => "application/json"]);
    }


    /**
     * @param Request $request
     * @param CommandEndPoint $endPoint
     * @return Response|JsonResponse
     * @throws AuthorizationException
     * @throws RouterException
     * @throws BindingResolutionException
     */
    private function executeCommandProcessor(Request $request, CommandEndPoint $endPoint): Response|JsonResponse
    {
        /** @var BaseCommandProcessor $processor */
        $processor = app($endPoint->getClassName());

        self::$currentUrl = $request->getPathInfo();


        $requestBody = $processor->decode($request);

        if ($this->authorize($processor->authorize($request)) === false) {
            throw new AuthorizationException();
        }

        $processor->validate($request, $requestBody);


        if ($processor instanceof CommandProcessor) {
            $processor->execute($request, $requestBody);

            return response()->make('');
        }

        if ($processor instanceof CommandResponseProcessor) {
            $response = $processor->execute($request, $requestBody);
            return response()->json(data: $response, headers: ["Content-Type" => "application/json"]);
        }

        if ($processor instanceof CommandRawResponseProcessor) {
            $response = $processor->execute($request, $requestBody);
            return response(content: $response, status: 200);
        }

        throw new RouterException();
    }


    /**
     * @param Request $request
     * @param CSVEndPoint $endPoint
     * @return BinaryFileResponse
     * @throws AuthorizationException
     */
    private function executeCSVProcessor(Request $request, CSVEndPoint $endPoint): BinaryFileResponse
    {
        /** @var CSVProcessor $processor */
        $processor = app($endPoint->getClassName());

        self::$currentUrl = $request->getPathInfo();

        $query = $this->extractor->extract($request->all());

        $processor->setHttpQuery($query);

        if ($this->authorize($processor->authorize($request)) === false) {
            throw new AuthorizationException();
        }

        $fileName = "{$processor->fileName()}.csv";

        $headers = [
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        ];

        $processor->execute($request);

        return response()->download($fileName, $fileName, $headers);
    }

    /**
     * @param PermissionDetails|null $permissionDetails
     * @return bool
     */
    private function authorize(?PermissionDetails $permissionDetails): bool
    {
        if ($permissionDetails === null || empty($permissionDetails->getGroups())) {
            return true;
        }

        $userPermissions = $this->getUserPermissions();

        if ($permissionDetails->getType()->getValue() === GroupType::AND()->getValue()) {
            foreach ($permissionDetails->getGroups() as $group) {
                $validGroup = $this->validateGroup($group, $userPermissions);
                if ($validGroup === false) {
                    return false;
                }
            }
            return true;
        }

        if ($permissionDetails->getType()->getValue() === GroupType::OR()->getValue()) {
            foreach ($permissionDetails->getGroups() as $group) {
                $validGroup = $this->validateGroup($group, $userPermissions);
                if ($validGroup === true) {
                    return true;
                }
            }
            return false;
        }

        return false;
    }

    /**
     * @param PermissionGroup $group
     * @param array $userPermissions
     * @return bool
     */
    private function validateGroup(PermissionGroup $group, array $userPermissions): bool
    {

        if ($group->getType()->getValue() === GroupType::AND()->getValue()) {
            /** @noinspection DuplicatedCode */
            foreach ($group->getPermissions() as $permission) {
                if ($permission instanceof FeaturesPermission) {
                    $validPermission = $this->isValidPermission($permission->getFeature(), $userPermissions);
                    if ($validPermission === false) {
                        return false;
                    }
                } else if ($permission instanceof CustomPermission) {
                    $validPermission = $permission->validate();
                    if ($validPermission === false) {
                        return false;
                    }
                }
            }
            return true;
        }

        if ($group->getType()->getValue() === GroupType::OR()->getValue()) {
            /** @noinspection DuplicatedCode */
            foreach ($group->getPermissions() as $permission) {
                if ($permission instanceof FeaturesPermission) {
                    $validPermission = $this->isValidPermission($permission->getFeature(), $userPermissions);
                    if ($validPermission === true) {
                        return true;
                    }
                } else if ($permission instanceof CustomPermission) {
                    $validPermission = $permission->validate();
                    if ($validPermission === true) {
                        return false;
                    }
                }
            }
            return false;
        }

        return false;
    }

    /**
     * @param string $permission
     * @param string[] $userPermissions
     * @return bool
     */
    private function isValidPermission(string $permission, array $userPermissions): bool
    {
        return in_array($permission, $userPermissions, true);
    }


    public static string $currentUrl;
}
