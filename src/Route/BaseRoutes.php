<?php /** @noinspection PhpUnused */

namespace LaravelModularDashboard\Route;


abstract class BaseRoutes
{

    protected Router $router;

    /**
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    abstract protected function resource(): string;

    abstract protected function apiVersion(): int;

    /** @noinspection PhpUnused */
    abstract public function register(): void;

    /** @noinspection PhpUnused */
    final protected function prefix(): string
    {
        return "/v{$this->apiVersion()}/{$this->resource()}";
    }
}
