<?php

namespace LaravelModularDashboard\EndPoints;

use LaravelModularDashboard\Route\RouteType;
use JetBrains\PhpStorm\Pure;

final class CSVEndPoint extends BaseEndPoint
{

    #[Pure]
    public function __construct(string $url, string $className)
    {
        parent::__construct(RouteType::GET(), $url, $className);
    }

}
