<?php


namespace LaravelModularDashboard\EndPoints;


use LaravelModularDashboard\Route\RouteType;

abstract class BaseEndPoint
{

    private string $url;
    private string $className;
    private RouteType $routeType;

    /**
     * BaseEndPoint constructor.
     * @param RouteType $routeType
     * @param string $url
     * @param string $className
     */
    public function __construct(RouteType $routeType, string $url, string $className)
    {
        $this->url = $url;
        $this->routeType = $routeType;
        $this->className = $className;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @return RouteType
     */
    public function getRouteType(): RouteType
    {
        return $this->routeType;
    }



}
