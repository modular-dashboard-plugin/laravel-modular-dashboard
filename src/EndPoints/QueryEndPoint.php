<?php

namespace LaravelModularDashboard\EndPoints;

use JetBrains\PhpStorm\Pure;
use LaravelModularDashboard\Route\RouteType;

final class QueryEndPoint extends BaseEndPoint
{

    #[Pure]
    public function __construct(string $url, string $className)
    {
        parent::__construct(RouteType::GET(), $url, $className);
    }


}
