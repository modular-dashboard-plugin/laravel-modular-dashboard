<?php


namespace LaravelModularDashboard\Processors;


use Illuminate\Http\Request;

abstract class CommandResponseProcessor extends BaseCommandProcessor
{


    /**
     * @param Request $row
     * @param $request
     * @return object|array
     */
    abstract public function execute(Request $row, $request): object|array;



}
