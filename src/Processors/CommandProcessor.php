<?php


namespace LaravelModularDashboard\Processors;


use Illuminate\Http\Request;

abstract class CommandProcessor extends BaseCommandProcessor
{


    /**
     * @param Request $row
     * @param $request
     * @return void
     */
    abstract public function execute(Request $row, $request): void;


}
