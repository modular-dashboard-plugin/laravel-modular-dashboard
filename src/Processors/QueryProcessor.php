<?php /** @noinspection PhpUnused */

namespace LaravelModularDashboard\Processors;


use Illuminate\Http\Request;
use LaravelModularDashboard\Permissions\PermissionDetails;
use LaravelModularDashboard\QueryExtractor\HttpQuery;

abstract class QueryProcessor
{


    private HttpQuery $httpQuery;


    /**
     * @param HttpQuery $httpQuery
     * @return void
     */
    final public function setHttpQuery(HttpQuery $httpQuery): void
    {
        $this->httpQuery = $httpQuery;
    }


    /**
     * @return HttpQuery
     * @noinspection PhpUnused
     */
    final protected function getHttpQuery(): HttpQuery
    {
        return $this->httpQuery;
    }


    /**
     * @param Request $row
     * @return PermissionDetails|null
     * @noinspection PhpUnusedParameterInspection
     */
    public function authorize(Request $row): ?PermissionDetails
    {
        return null;
    }


    /**
     * @param Request $row
     * @return object|array
     */
    abstract public function execute(Request $row): object|array;


}
