<?php

/** @noinspection PhpMultipleClassDeclarationsInspection */


namespace LaravelModularDashboard\Processors;


use LaravelModularDashboard\Exceptions\RouterException;
use LaravelModularDashboard\Permissions\PermissionDetails;
use Illuminate\Http\Request;
use JsonException;
use JsonMapper;
use JsonMapper_Exception;
use LaravelModularDashboard\AppLogger;
use ReflectionClass;
use ReflectionException;

abstract class BaseCommandProcessor
{


    /**
     * @return string|null
     */
    abstract public function setRequestType(): ?string;

    /**
     * @param Request $row
     * @param $request
     */
    public function validate(Request $row, $request): void
    {
    }


    /**
     * @param Request $row
     * @return PermissionDetails|null
     * @noinspection PhpUnusedParameterInspection
     */
    public function authorize(Request $row): ?PermissionDetails
    {
        return null;
    }


    /**
     * @param Request $row
     * @return mixed
     * @throws RouterException
     */
    final public function decode(Request $row): mixed
    {
        if (!is_string($row->getContent()) || empty($row->getContent()))
        {
            return null;
        }

        if ($this->setRequestType() === null)
        {
            throw new RouterException("Request type not implemented");
        }
        try
        {
            $content = $row->getContent();

            try
            {
                $request = json_decode($content, false, 512, JSON_THROW_ON_ERROR);
            } catch (JsonException)
            {
                $array = [];
                parse_str(urldecode($content), $array);
                $content = json_encode($array, JSON_THROW_ON_ERROR);
                $request = json_decode($content, false, 512, JSON_THROW_ON_ERROR);
            }

            $mapper = new JsonMapper();
            $reflect = new ReflectionClass($this->setRequestType());
            $object = $reflect->newInstanceWithoutConstructor();
            return $mapper->map((object)$request, $object);
        } catch (JsonMapper_Exception|JsonException|ReflectionException $e)
        {
            AppLogger::getInstance()->error($e->getMessage(), $e->getTrace());
            throw new RouterException("Can't parse request.");
        }
    }
}
