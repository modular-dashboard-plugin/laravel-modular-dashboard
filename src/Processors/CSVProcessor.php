<?php /** @noinspection PhpUnused */


namespace LaravelModularDashboard\Processors;


use Illuminate\Http\Request;
use LaravelModularDashboard\Permissions\PermissionDetails;
use LaravelModularDashboard\QueryExtractor\HttpQuery;

abstract class CSVProcessor
{

    private HttpQuery $httpQuery;


    /**
     * @param HttpQuery $httpQuery
     * @return void
     */
    final public function setHttpQuery(HttpQuery $httpQuery): void
    {
        $this->httpQuery = $httpQuery;
    }


    /** @noinspection PhpUnused */
    final protected function getHttpQuery(): HttpQuery
    {
        return $this->httpQuery;
    }


    /**
     * @return string
     */
    abstract public function fileName(): string;


    /**
     * @param Request $row
     * @return PermissionDetails|null
     * @noinspection PhpUnusedParameterInspection
     */
    public function authorize(Request $row): ?PermissionDetails
    {
        return null;
    }


    /**
     * @param Request $row
     * @return resource
     */
    abstract public function execute(Request $row);

    /**
     * @param array $columns
     * @param array $rows
     * @return void
     */
    final public function convertToCSV(array $columns, array $rows): void
    {
        $file = fopen("{$this->fileName()}.csv", 'wb');

        $tableHeader = array_values($columns);

        fputcsv($file, $tableHeader);

        foreach ($rows as $row) {
            $data = [];

            foreach (array_keys($columns) as $key) {
                /** @noinspection PhpConditionAlreadyCheckedInspection */
                if (!isset($row[$key]) || $row[$key] === null) {
                    $data[$key] = '-';
                } else {
                    $chunk = trim($row[$key]);
                    if (empty($chunk) || $chunk === 'null' || $chunk === 'NULL') {
                        $data[$key] = '-';
                    } else {
                        $data[$key] = $chunk;
                    }
                }
            }

            fputcsv($file, $data);
        }
        fclose($file);
    }
}
