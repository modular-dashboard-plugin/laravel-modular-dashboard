<?php

namespace LaravelModularDashboard\Processors;


use Illuminate\Http\Request;

abstract class CommandRawResponseProcessor extends BaseCommandProcessor
{


    /**
     * @param Request $row
     * @param $request
     * @return string
     */
    abstract public function execute(Request $row, $request): string;



}
