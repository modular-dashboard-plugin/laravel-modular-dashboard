<?php

namespace LaravelModularDashboard\Permissions;
use MyCLabs\Enum\Enum;

final class GroupType extends Enum
{

    private const AND = 'AND';
    private const OR = 'OR';

    /**
     * @return GroupType
     */
    public static function AND(): GroupType
    {
        return new self(self::AND);
    }
    /**
     * @return GroupType
     */
    public static function OR(): GroupType
    {
        return new self(self::OR);
    }
}
