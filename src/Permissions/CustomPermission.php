<?php

namespace LaravelModularDashboard\Permissions;

final class CustomPermission extends Permission
{

    private $callback;

    /**
     * @param callable $callback
     */
    public function __construct(callable $callback)
    {
        $this->callback = $callback;
    }

    public function validate(): bool
    {
        return call_user_func($this->callback);
    }


}