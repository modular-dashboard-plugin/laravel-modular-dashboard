<?php /** @noinspection PhpUnused */

namespace LaravelModularDashboard\Permissions;

final class PermissionDetails
{

    /**
     * @var PermissionGroup[]
     */
    private array $groups;
    private GroupType $type;

    /**
     * @param PermissionGroup[] $groups
     */
    public function __construct(array $groups, GroupType $type)
    {
        $this->groups = $groups;
        $this->type = $type;
    }

    /**
     * @return PermissionGroup[]
     */
    public function getGroups(): array
    {
        return $this->groups;
    }


    /**
     * @return GroupType
     */
    public function getType(): GroupType
    {
        return $this->type;
    }

    /**
     * @param string $permission
     * @return PermissionDetails
     */
    public static function haveThis(string $permission): PermissionDetails
    {
        return new PermissionDetails([PermissionGroup::haveThis($permission)], GroupType::AND());
    }


    /**
     * @param string[] $permissions
     * @return PermissionDetails
     */
    public static function haveAllOf(array $permissions): PermissionDetails
    {
        return new PermissionDetails([PermissionGroup::haveAllOf($permissions)], GroupType::AND());
    }


    /**
     * @param string[] $permissions
     * @return PermissionDetails
     */
    public static function haveOneOf(array $permissions): PermissionDetails
    {
        return new PermissionDetails([PermissionGroup::haveOneOf($permissions)], GroupType::AND());
    }

    /**
     * @param PermissionGroup[] $groups
     * @param GroupType|null $type
     * @return PermissionDetails
     */
    public static function have(array $groups, ?GroupType $type = null): PermissionDetails
    {
        if ($type === null)
        {
            $type = GroupType::OR();
        }
        return new PermissionDetails($groups, $type);
    }


    /**
     * @param CustomPermission $permission
     * @return PermissionDetails
     */
    public static function customHaveThis(CustomPermission $permission): PermissionDetails
    {
        return new PermissionDetails([PermissionGroup::customHaveThis($permission)], GroupType::AND());
    }


    /**
     * @param CustomPermission[] $permissions
     * @return PermissionDetails
     */
    public static function customerHaveAllOf(array $permissions): PermissionDetails
    {

        return new PermissionDetails([PermissionGroup::customerHaveAllOf($permissions)], GroupType::AND());
    }


    /**
     * @param CustomPermission[] $permissions
     * @return PermissionDetails
     */
    public static function customerHaveOneOf(array $permissions): PermissionDetails
    {

        return new PermissionDetails([PermissionGroup::customerHaveOneOf($permissions)], GroupType::OR());
    }

}
