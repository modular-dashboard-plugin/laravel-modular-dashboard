<?php

namespace LaravelModularDashboard\Permissions;

use Cocur\Chain\Chain;

final class PermissionGroup
{

    /**
     * @var Permission[]
     */
    private array $permissions;
    private GroupType $type;

    /**
     * @param Permission[] $permissions
     * @param GroupType $type
     */
    public function __construct(array $permissions, GroupType $type)
    {
        $this->permissions = $permissions;
        $this->type = $type;
    }

    /**
     * @return Permission[]
     */
    public function getPermissions(): array
    {
        return $this->permissions;
    }

    /**
     * @return GroupType
     */
    public function getType(): GroupType
    {
        return $this->type;
    }


    /**
     * @param string $permission
     * @return PermissionGroup
     */
    public static function haveThis(string $permission): PermissionGroup
    {
        $features = [
            FeaturesPermission::create($permission)
        ];
        return new PermissionGroup($features, GroupType::AND());
    }


    /**
     * @param string[] $permissions
     * @return PermissionGroup
     */
    public static function haveAllOf(array $permissions): PermissionGroup
    {
        $features = Chain::create($permissions)->map(
            fn(string $permission) => FeaturesPermission::create($permission)
        )->array;
        return new PermissionGroup($features, GroupType::AND());
    }


    /**
     * @param string[] $permissions
     * @return PermissionGroup
     */
    public static function haveOneOf(array $permissions): PermissionGroup
    {
        $features = Chain::create($permissions)->map(
            fn(string $permission) => FeaturesPermission::create($permission)
        )->array;
        return new PermissionGroup($features, GroupType::OR());
    }


    public static function customHaveThis(CustomPermission $permission): PermissionGroup
    {
        $features = [
            $permission
        ];
        return new PermissionGroup($features, GroupType::AND());
    }


    /**
     * @param CustomPermission[] $permissions
     * @return PermissionGroup
     */
    public static function customerHaveAllOf(array $permissions): PermissionGroup
    {

        return new PermissionGroup($permissions, GroupType::AND());
    }


    /**
     * @param CustomPermission[] $permissions
     * @return PermissionGroup
     */
    public static function customerHaveOneOf(array $permissions): PermissionGroup
    {

        return new PermissionGroup($permissions, GroupType::OR());
    }


}
