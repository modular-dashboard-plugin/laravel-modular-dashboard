<?php

namespace LaravelModularDashboard\Permissions;

use JetBrains\PhpStorm\Pure;

final class FeaturesPermission extends Permission
{
    private string $feature;

    /**
     * @param string $feature
     */
    private function __construct(string $feature)
    {
        $this->feature = $feature;
    }

    /**
     * @return string
     */
    public function getFeature(): string
    {
        return $this->feature;
    }


    /**
     * @param string $feature
     * @return FeaturesPermission
     */
    #[Pure]
    public static function create(string $feature): FeaturesPermission
    {
        return new FeaturesPermission($feature);
    }

}