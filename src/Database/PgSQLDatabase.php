<?php


namespace LaravelModularDashboard\Database;


use LaravelModularDashboard\AppLogger;
use LaravelModularDashboard\Exceptions\DatabaseException;
use Exception;

final class PgSQLDatabase extends DatabaseConnection
{


    /**
     * @var resource|null
     */
    private $connection;


    /**
     * @throws DatabaseException
     */
    public function __construct(IDatabaseConnector $connector)
    {
        try
        {
            if ($connector->fullPath() !== null)
            {
                $this->connection = pg_connect($connector->fullPath());
            }
            else
            {
                $this->connection = pg_connect("postgres://{$connector->username()}:{$connector->password()}@{$connector->host()}:{$connector->port()}/{$connector->database()}");
            }
        } catch (Exception $exception)
        {
            AppLogger::getInstance()->error('Database connection failed: ' . $exception->getMessage(), $exception->getTrace());
            throw new DatabaseException("Internal error exception. Please contact support");
        }
    }


    public function closeConnection(): void
    {
        if ($this->connection !== null)
        {
            try
            {
                pg_close($this->connection);
            } catch (Exception $exception)
            {
                AppLogger::getInstance()->error('Database connection close failed: ' . pg_last_error($this->connection) . " - Exception {$exception->getMessage()}", $exception->getTrace());
            }
        }
    }

    /**
     * @param string $query
     * @return void
     * @throws DatabaseException
     */
    public function execute(string $query): void
    {
        $this->executeInternal($query);
    }


    /**
     * @param string $query
     * @return array|null
     * @throws DatabaseException
     */
    public function getRow(string $query): ?array
    {
        $result = $this->executeInternal($query);

        /** @var array|false $data */
        $data = pg_fetch_array($result, null, PGSQL_ASSOC);

        if ($data === FALSE)
        {
            $this->logError($query);
            return null;
        }

        return $data;
    }

    /**
     * @param string $query
     * @return array
     * @throws DatabaseException
     */
    public function getArray(string $query): array
    {
        $result = $this->executeInternal($query);

        /** @var array|false $data */
        $data = pg_fetch_all($result);

        if ($data === FALSE)
        {
            $this->logError($query);
            return [];
        }

        return $data;
    }


    /**
     * @param string|null $string $string
     * @return string|null
     */
    public function escapeCharacters(?string $string): ?string
    {
        if ($string === null)
        {
            return null;
        }
        return pg_escape_string($this->connection, $string);
    }


    /**
     * @param string $query
     * @return resource
     * @throws DatabaseException
     */
    private function executeInternal(string $query)
    {
        if (getenv('APP_ENV') !== null)
        {
            AppLogger::getInstance()->debug("Database query:  $query");
        }

        try
        {
            $result = pg_query($this->connection, $query);

            if ($result === false)
            {
                $this->logError($query);
                throw new DatabaseException("Internal error exception. Please contact support");
            }
            return $result;
        } catch (Exception $exception)
        {
            AppLogger::getInstance()->error("{$exception->getMessage()} -- Erroneous query:  $query", $exception->getTrace());
            throw new DatabaseException("Internal error exception. Please contact support");
        }
    }

    /**
     * @param string $query
     */
    private function logError(string $query): void
    {
        $mysqlError = pg_last_error($this->connection);
        AppLogger::getInstance()->error("$mysqlError -- Erroneous query:  $query");
    }


    /** @noinspection SqlNoDataSourceInspection */
    protected function insertQuery(string $table, string $fields, string $values, string $key, ?string $editableFields): string
    {
        if ($editableFields !== null)
        {
            return "INSERT INTO $table ($fields) VALUES $values ON CONFLICT ($key) DO UPDATE SET $editableFields;";
        }

        return "INSERT INTO $table ($fields) VALUES $values ON CONFLICT ($key) DO NOTHING;";
    }

    protected function tempValueStatement(string $name): string
    {
        return "$name = EXCLUDED.$name";
    }
}
