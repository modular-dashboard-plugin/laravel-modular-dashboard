<?php

namespace LaravelModularDashboard\Database;

use Doctrine\Common\Annotations\Annotation\NamedArgumentConstructor;

/**
 * @Annotation
 * @NamedArgumentConstructor
 */
class Field
{

    public function __construct(bool $nullable = false, bool $editable = false, bool $escape = false, bool $key = false)
    {
        $this->nullable = $nullable;
        $this->editable = $editable;
        $this->escape = $escape;
        $this->key = $key;
    }

    public bool $nullable;
    public bool $editable;
    public bool $escape;
    public bool $key;


}
