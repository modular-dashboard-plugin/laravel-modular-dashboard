<?php

namespace LaravelModularDashboard\Database;

use DateInterval;
use LaravelModularDashboard\QueryExtractor\Filters\DateFilter;

final class PgSQLQueryDatabase extends QueryDatabase
{

    protected function listAggregationFunctions(): array
    {
        return [
            'ARRAY_AGG',
            'ARRAY_AGG',
            'AVG',
            'BIT_AND',
            'BIT_OR',
            'BOOL_AND',
            'BOOL_OR',
            'COUNT',
            'COUNT',
            'EVERY',
            'JSON_AGG',
            'JSONB_AGG',
            'JSON_OBJECT_AGG',
            'JSONB_OBJECT_AGG',
            'MAX',
            'MIN',
            'STRING_AGG',
            'SUM',
            'XMLAGG',
        ];
    }

    protected function getDateFilterValue(DateFilter $filter): string
    {
        $start = $filter->getStart()->format('Y-m-d H:i:s');
        $end = $filter->getEnd()->add(new DateInterval('P1D'))->format('Y-m-d H:i:s');

        return "'$start'::timestamp AND '$end'::timestamp";
    }

    protected function getDateFilterQueryString(string $column, string $value): string
    {
        return " AND $column::timestamp BETWEEN $value";
    }

    protected function getCountFromQueryString(DatabaseQuery $query, string $where = "", string $having = ""): string
    {
        $queryString = preg_replace('/(SELECT|select)([\S\s]*?)(FROM|from)/', "SELECT COUNT(DISTINCT {$query->getDistinctField()}) as count FROM", $query->getQuery(), 1);

        return "$queryString $where $having;";
    }
}
