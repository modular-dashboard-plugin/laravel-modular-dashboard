<?php


namespace LaravelModularDashboard\Database;


use LaravelModularDashboard\Exceptions\DatabaseException;
use mysqli;
use mysqli_result;
use LaravelModularDashboard\AppLogger;

final class MySQLDatabase extends DatabaseConnection
{


    private ?mysqli $connection;


    /**
     * @throws DatabaseException
     */
    public function __construct(IDatabaseConnector $connector)
    {


        $this->connection = mysqli_connect(
            hostname: $connector->host(),
            username: $connector->username(),
            password: $connector->password(),
            database: $connector->database(),
            port: $connector->port()
        );

        if (!$this->connection)
        {
            AppLogger::getInstance()->error('Database connection failed: ' . mysqli_error($this->connection));
            throw new DatabaseException("Internal error exception. Please contact support");
        }

    }


    public function closeConnection(): void
    {
        if ($this->connection !== null)
        {
            mysqli_close($this->connection);
        }
    }

    /**
     * @param string $query
     * @return void
     * @throws DatabaseException
     */
    public function execute(string $query): void
    {
        $this->executeInternal($query);
    }

    /**
     * @param string $query
     * @return array|null
     * @throws DatabaseException
     */
    public function getRow(string $query): ?array
    {
        $result = $this->executeInternal($query);
        if ($result instanceof mysqli_result)
        {
            return $result->fetch_array(MYSQLI_ASSOC);
        }
        throw new DatabaseException("Internal error exception. Please contact support");
    }


    /**
     * Generates an array out of the resultSet
     * @param string $query
     * @return array
     * @throws DatabaseException
     */
    public function getArray(string $query): array
    {
        $result = $this->executeInternal($query);
        if ($result instanceof mysqli_result)
        {
            return $result->fetch_all(MYSQLI_ASSOC);
        }
        throw new DatabaseException("Internal error exception. Please contact support");
    }


    /**
     * @param string|null $string $string
     * @return string|null
     */
    public function escapeCharacters(?string $string): ?string
    {
        if ($string === null)
        {
            return null;
        }
        return mysqli_real_escape_string($this->connection, $string);
    }


    /**
     * @param string $query
     * @return mysqli_result|bool
     * @throws DatabaseException
     */
    private function executeInternal(string $query): mysqli_result|bool
    {
        if (getenv('APP_ENV') === 'local')
        {
            AppLogger::getInstance()->debug("Database query:  $query");
        }

        $result = mysqli_query($this->connection, $query);

        if ($result === false)
        {
            $this->logError($query);
            throw new DatabaseException("Internal error exception. Please contact support");
        }
        return $result;
    }

    /**
     * @param string $query
     */
    private function logError(string $query): void
    {
        $error = mysqli_error($this->connection);
        AppLogger::getInstance()->error("$error -- Erroneous query:  $query");
    }


    /** @noinspection SqlNoDataSourceInspection */
    protected function insertQuery(string $table, string $fields, string $values, string $key, ?string $editableFields): string
    {
        if ($editableFields !== null)
        {
            return "INSERT INTO $table ($fields) VALUES $values ON DUPLICATE KEY UPDATE $editableFields;";
        }

        return "INSERT IGNORE INTO $table ($fields) VALUES $values;";
    }

    protected function tempValueStatement(string $name): string
    {
        return "$name = VALUES($name)";
    }

}
