<?php /** @noinspection PhpUnused */


namespace LaravelModularDashboard\Database;


final class DatabaseResult
{
    private array $data;
    private int $count;

    /**
     * DatabaseResult constructor.
     * @param array $data
     * @param int $count
     */
    public function __construct(array $data, int $count)
    {
        $this->data = $data;
        $this->count = $count;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }




}
