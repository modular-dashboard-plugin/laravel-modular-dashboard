<?php /** @noinspection PhpUnused */

namespace LaravelModularDashboard\Database;

final class DatabaseQuery
{

    private string $query;
    private string $distinctField;
    private ?string $defaultField;
    private string $defaultDirection;
    private bool $withWhere;

    /**
     * @param string $query
     * @param string $distinctField
     * @param string|null $defaultField
     * @param string $defaultDirection
     * @param bool $withWhere
     */
    public function __construct(string $query, string $distinctField, ?string $defaultField = null, string $defaultDirection = "ASC", bool $withWhere = false)
    {
        $this->query = $query;
        $this->distinctField = $distinctField;
        $this->defaultField = $defaultField;
        $this->defaultDirection = $defaultDirection;
        $this->withWhere = $withWhere;
    }


    /**
     * @return string|null
     */
    public function getQuery(): ?string
    {
        return $this->query;
    }

    /**
     * @return string|null
     */
    public function getDefaultSort(): ?string
    {
        if ($this->defaultField !== null)
        {
            return "$this->defaultField $this->defaultDirection";
        }

        return null;

    }

    /**
     * @return bool
     */
    public function getWithWhere(): bool
    {
        return $this->withWhere;
    }

    /**
     * @return string
     */
    public function getDistinctField(): string
    {
        return $this->distinctField;
    }

}
