<?php

namespace LaravelModularDashboard\Database;

use ReflectionProperty;

class FieldEntity
{

    private string $name;
    private ?Field $annotation;
    private ReflectionProperty $property;


    /**
     * @param string $name
     * @param Field|null $annotation
     * @param ReflectionProperty $property
     */
    public function __construct(string $name, ?Field $annotation, ReflectionProperty $property)
    {
        $this->name = $name;
        $this->annotation = $annotation;
        $this->property = $property;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Field|null
     */
    public function getAnnotation(): ?Field
    {
        return $this->annotation;
    }

    /**
     * @return ReflectionProperty
     */
    public function getProperty(): ReflectionProperty
    {
        return $this->property;
    }





}
