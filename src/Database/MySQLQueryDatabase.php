<?php

namespace LaravelModularDashboard\Database;

use DateInterval;
use LaravelModularDashboard\QueryExtractor\Filters\DateFilter;

final class MySQLQueryDatabase extends QueryDatabase
{


    /** @noinspection SpellCheckingInspection */
    protected function listAggregationFunctions(): array
    {
        return [
            "AVG",
            "BIT_AND",
            "BIT_OR",
            "BIT_XOR",
            "COUNT",
            "COUNT",
            "GROUP_CONCAT",
            "JSON_ARRAYAGG",
            "JSON_OBJECTAGG",
            "MAX",
            "MIN",
            "STD",
            "STDDEV",
            "STDDEV_POP",
            "STDDEV_SAMP",
            "SUM",
            "VAR_POP",
            "VAR_SAMP",
            "VARIANCE"
        ];
    }

    protected function getDateFilterValue(DateFilter $filter): string
    {
        $start = $filter->getStart()->getTimestamp();
        $end = $filter->getEnd()->add(new DateInterval('P1D'))->getTimestamp();
        return "$start AND $end";
    }

    protected function getDateFilterQueryString(string $column, string $value): string
    {
        return " AND UNIX_TIMESTAMP($column) BETWEEN $value";
    }

    protected function getCountFromQueryString(DatabaseQuery $query, string $where = "", string $having = ""): string
    {
        $queryString = preg_replace('/(SELECT|select)([\S\s]*?)(FROM|from)/', "SELECT DISTINCT {$query->getDistinctField()} FROM", $query->getQuery(), 1);
        $group = " GROUP BY {$query->getDistinctField()} \n";
        $subQuery = "($queryString $where $group $having)";
        return "SELECT COUNT(*) as count FROM $subQuery as countable;";
    }
}
