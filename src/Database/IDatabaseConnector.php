<?php

namespace LaravelModularDashboard\Database;

interface IDatabaseConnector
{

    public function host(): string;

    public function username(): string;

    public function password(): string;

    public function database(): string;

    public function port(): int;

    public function fullPath(): ?string;
}
