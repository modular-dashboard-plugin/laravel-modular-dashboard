<?php /** @noinspection PhpUnused */

namespace LaravelModularDashboard\Database;

use LaravelModularDashboard\Exceptions\DatabaseException;

use LaravelModularDashboard\QueryExtractor\Filter;
use LaravelModularDashboard\QueryExtractor\Filters\DateFilter;
use LaravelModularDashboard\QueryExtractor\Filters\ListFilter;
use LaravelModularDashboard\QueryExtractor\Filters\NumericFilter;
use LaravelModularDashboard\QueryExtractor\Filters\StringFilter;
use LaravelModularDashboard\QueryExtractor\FilterType;
use LaravelModularDashboard\QueryExtractor\HttpQuery;
use JetBrains\PhpStorm\ArrayShape;

abstract class QueryDatabase
{

    protected DatabaseConnection $database;

    /**
     * @param DatabaseConnection $database
     */
    public function __construct(DatabaseConnection $database)
    {
        $this->database = $database;
    }

    /**
     * @param DatabaseQuery $databaseQuery
     * @param HttpQuery|null $httpQuery
     * @return DatabaseResult
     * @throws DatabaseException
     */
    final public function query(DatabaseQuery $databaseQuery, ?HttpQuery $httpQuery): DatabaseResult
    {


        $fields = $this->parseQuery($databaseQuery->getQuery());

        if ($httpQuery !== null && empty($httpQuery->getSorting()) === false) {
            $sortingFields = [];
            foreach ($httpQuery->getSorting() as $sort) {
                $sortingFields[] = " {$sort->getField()} {$sort->getDirection()} ";
            }

            $sortingFieldsString = implode(',', $sortingFields);

            $sortingString = "ORDER BY $sortingFieldsString";
        } else if ($databaseQuery->getDefaultSort() !== null) {
            $sortingString = "ORDER BY {$databaseQuery->getDefaultSort()}";
        } else {
            $sortingString = "";
        }

        if ($httpQuery !== null && $httpQuery->getPaging() !== null) {
            $pagingString = "LIMIT {$httpQuery->getPaging()->getPageSize()} OFFSET {$httpQuery->getPaging()->getOffset()}";
        } else {
            $pagingString = "";
        }

        $groupByString = "";

        if (count($fields['aggregateFields']) > 0) {
            $groupedFields = array_values($fields['fields']);
            $groupedFieldsString = implode(", ", $groupedFields);

            $groupByString = " GROUP BY $groupedFieldsString";
        }

        if ($httpQuery === null) {
            $dataQuery = "{$databaseQuery->getQuery()} $groupByString $sortingString $pagingString;";
            $countQuery = $this->getCountFromQueryString($databaseQuery);

            return $this->getResult($dataQuery, $countQuery);
        }


        $whereFiltersString = "";
        $havingFiltersString = "";
        foreach ($httpQuery->getFilters() as $filter) {
            if (isset($fields['fields'][$filter->getField()])) {
                $columnName = $fields['fields'][$filter->getField()];
                $whereFiltersString .= $this->addFilterToQueryString($columnName, $filter);
            } else if (isset($fields['aggregateFields'][$filter->getField()])) {
                $columnName = $fields['aggregateFields'][$filter->getField()];
                $havingFiltersString .= $this->addFilterToQueryString($columnName, $filter);
            } else {
                throw new DatabaseException("Field is not configured {$filter->getField()}");
            }
        }

        if ($databaseQuery->getWithWhere() === false && empty(trim($whereFiltersString)) === false) {
            $whereFiltersString = " WHERE {$this->removeFirstAdd($whereFiltersString)}";
        }


        if (empty(trim($havingFiltersString)) === false) {
            $havingFiltersString = " HAVING {$this->removeFirstAdd($havingFiltersString)}";
        }


        $dataQuery = "{$databaseQuery->getQuery()} $whereFiltersString $groupByString $havingFiltersString $sortingString $pagingString;";
        $countQuery = $this->getCountFromQueryString($databaseQuery, $whereFiltersString, $havingFiltersString);


        return $this->getResult($dataQuery, $countQuery);
    }


    /**
     * @param string $content
     * @return string
     */
    private function removeFirstAdd(string $content): string
    {
        $from = '/' . preg_quote(" AND", '/') . '/';

        return preg_replace($from, "", $content, 1);
    }


    /**
     * @param string $column
     * @param Filter $filter
     * @return string
     */
    final protected function addFilterToQueryString(string $column, Filter $filter): string
    {
        $filterType = $filter->getFilterType()->getValue();

        $value = $this->getRowFilterValue($filter);

        if ($value === null) {
            return "";
        }

        return match ($filterType) {
            FilterType::EQUAL()->getValue() => " AND $column = $value",
            FilterType::NOT_EQUAL()->getValue() => " AND $column != $value",
            FilterType::LIKE()->getValue(), FilterType::START_LIKE()->getValue() => " AND LOWER($column) LIKE LOWER($value)",
            FilterType::GREATER_THEN()->getValue() => " AND $column > $value",
            FilterType::GREATER_THEN_OR_EQUAL()->getValue() => " AND $column >= $value",
            FilterType::LESS_THEN()->getValue() => " AND $column < $value",
            FilterType::LESS_THEN_OR_EQUAL()->getValue() => " AND $column <= $value",
            FilterType::IN()->getValue() => " AND $column IN $value",
            FilterType::IN_RANG()->getValue() => $this->getDateFilterQueryString($column, $value),
            default => "",
        };
    }


    /**
     * @param string $query
     * @return array
     * @throws DatabaseException
     */
    #[ArrayShape(['fields' => "array", 'aggregateFields' => "array"])]
    private function parseQuery(string $query): array
    {
        $queryRows = $this->getRowsFromQueryString($query);

        $fields = [];
        $aggregateFields = [];
        foreach ($queryRows as $row) {
            $field = $this->parseRow($row);
            if ($field === null) {
                throw new DatabaseException("Query Exception. Can't parse $row");
            }

            if ($field['aggregate']) {
                $aggregateFields[$field['alias']] = $field['field'];
            } else {
                $fields[$field['alias']] = $field['field'];
            }
        }

        return [
            'fields' => $fields,
            'aggregateFields' => $aggregateFields,
        ];

    }


    /**
     * @param string $query
     * @return array
     * @throws DatabaseException
     */
    private function getRowsFromQueryString(string $query): array
    {
        $matches = [];

        preg_match('/(SELECT|select)([\S\s]*?)(FROM|from)/', $query, $matches);

        if (count($matches) < 2) {
            throw new DatabaseException("can't parse this query -> $query");
        }

        $queryString = str_replace(array('SELECT', 'FROM', 'select', 'from', '  ', "\n", "\t"), array('', '', '', '', ' ', '', ''), $matches[0]);

        $queryChars = str_split($queryString . ',');


        $fields = [];
        $currentField = '';
        foreach ($queryChars as $char) {

            if ($char === ',' && $this->isValidField($currentField)) {
                $fields[] = trim($currentField);
                $currentField = '';
            } else {
                $currentField .= $char;
            }
        }

        return $fields;
    }

    /**
     * @param string $row
     * @return array|null
     */
    private function parseRow(string $row): ?array
    {

        if (!$this->isValidField($row)) {
            return null;
        }

        $parts = preg_split("/ AS | as /i", $row);

        if (count($parts) === 2) {

            $field = trim($parts[0]);
            $alias = trim($parts[1]);

            if ($this->checkForAggregateFunctions($field)) {
                return ['alias' => $alias, 'field' => $field, 'aggregate' => false];
            }

            return ['alias' => $alias, 'field' => $field, 'aggregate' => true];
        }

        if (count($parts) === 1) {
            $field = trim($parts[0]);
            $alias = preg_replace('/^[a-z_]*\./', '', $field);
            return ['alias' => $alias, 'field' => $field, 'aggregate' => false];
        }

        return null;
    }

    /**
     * @param string $currentField
     * @return bool
     */
    private function isValidField(string $currentField): bool
    {
        return preg_match('/^( *[a-zA-Z0-9. _-]* *(AS|as) *[a-zA-Z0-9_-]* *)$/', $currentField)
            || preg_match('/^( *[a-zA-Z0-9. _-]* *)$/', $currentField)
            || preg_match('/^( *[A-Za-z0-9_ ]*\([a-zA-Z0-9():\'., ><=_+-]*\)* *(AS|as) *[a-zA-Z0-9_-]* *)$/', $currentField);
    }

    /**
     * @param string $field
     * @return bool
     */
    private function checkForAggregateFunctions(string $field): bool
    {
        $field = strtoupper($field);
        foreach ($this->listAggregationFunctions() as $function) {
            $function = strtoupper($function);
            if (str_contains($field, "$function(")) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return string[]
     */
    abstract protected function listAggregationFunctions(): array;

    /**
     * @param string $dataQuery
     * @param string $countQuery
     * @return DatabaseResult
     * @throws DatabaseException
     */
    private function getResult(string $dataQuery, string $countQuery): DatabaseResult
    {
        $data = $this->database->getArray($dataQuery);

        $count = 0;

        $result = $this->database->getRow($countQuery);

        if ($result !== null) {
            $count = $result['count'];
        }

        return new DatabaseResult($data, $count);
    }


    final protected function getRowFilterValue(Filter $filter): ?string
    {
        $filterType = $filter->getFilterType()->getValue();

        if ($filter instanceof StringFilter) {
            if ($filterType === FilterType::LIKE()->getValue()) {
                $value = "'%{$this->database->escapeCharacters($filter->getValue())}%'";
            } else if ($filterType === FilterType::START_LIKE()->getValue()) {
                $value = "'{$this->database->escapeCharacters($filter->getValue())}%'";
            } else {
                $value = "'{$this->database->escapeCharacters($filter->getValue())}'";
            }
        } else if ($filter instanceof ListFilter) {
            $values = implode("','", $filter->getValue());
            $value = "('$values')";
        } else if ($filter instanceof NumericFilter) {
            $value = $filter->getValue();
        } else if ($filter instanceof DateFilter) {
            $value = $this->getDateFilterValue($filter);
        } else {
            return null;
        }

        return $value;
    }

    abstract protected function getDateFilterValue(DateFilter $filter): string;

    abstract protected function getDateFilterQueryString(string $column, string $value): string;

    abstract protected function getCountFromQueryString(DatabaseQuery $query, string $where = "", string $having = ""): string;
}
