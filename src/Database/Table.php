<?php

namespace LaravelModularDashboard\Database;

use Doctrine\Common\Annotations\Annotation\NamedArgumentConstructor;
use Doctrine\Common\Annotations\Annotation\Required;

/**
 * @Annotation
 * @NamedArgumentConstructor
 */
class Table
{

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @Required()
     */
    public string $name;


}
