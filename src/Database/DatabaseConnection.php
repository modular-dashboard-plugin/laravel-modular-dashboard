<?php

namespace LaravelModularDashboard\Database;

use Doctrine\Common\Annotations\AnnotationReader;
use JsonMapper;
use JsonMapper_Exception;
use LaravelModularDashboard\Exceptions\DatabaseException;
use ReflectionClass;
use ReflectionException;

abstract class DatabaseConnection
{


    /** @noinspection PhpUnused */
    abstract public function closeConnection(): void;

    /**
     * @param string $query
     * @throws DatabaseException
     */
    abstract public function execute(string $query): void;

    /**
     * @param string $query
     * @return array|null
     * @throws DatabaseException
     */
    abstract public function getRow(string $query): ?array;

    /**
     * Generates an array out of the resultSet
     * @param string $query
     * @return array
     * @throws DatabaseException
     */
    abstract public function getArray(string $query): array;


    /**
     * @param string|null $string $string
     * @return string|null
     */
    abstract public function escapeCharacters(?string $string): ?string;


    abstract protected function insertQuery(string $table, string $fields, string $values, string $key, ?string $editableFields): string;

    abstract protected function tempValueStatement(string $name): string;

    /**
     * @param string|null $value
     * @return string
     * @noinspection PhpUnused
     */
    final public function nullableString(?string $value): string
    {
        if ($value === null)
        {
            return 'NULL';
        }

        return "'$value'";
    }


    /**
     * @param string $class
     * @param object ...$entities
     * @return void
     * @throws DatabaseException
     * @noinspection PhpUnused
     */
    final public function insert(string $class, object ...$entities): void
    {
        $query = $this->buildInsertQuery($class, ...$entities);
        $this->execute($query);
    }


    /**
     * @param string $class
     * @param object ...$entities
     * @return string
     * @throws DatabaseException
     */
    private function buildInsertQuery(string $class, object ...$entities): string
    {
        try
        {
            $reflect = new ReflectionClass($class);
        } catch (ReflectionException $e)
        {
            throw new DatabaseException($e->getMessage());
        }

        $reader = new AnnotationReader();
        /** @noinspection PhpParamsInspection */
        $classAnnotation = $reader->getClassAnnotation($reflect, Table::class);

        if ($classAnnotation === null)
        {
            throw new DatabaseException("table $class annotation is missing");
        }

        $tableName = $classAnnotation->name;

        $props = $reflect->getProperties();

        $fields = [];

        $key = null;
        foreach ($props as $property)
        {
            $reader = new AnnotationReader();
            /** @noinspection PhpParamsInspection */
            $fieldAnnotation = $reader->getPropertyAnnotation($property, Field::class);

            if ($fieldAnnotation === null)
            {
                continue;
            }

            $fields[] = new FieldEntity(
                name: $property->getName(),
                annotation: $fieldAnnotation,
                property: $property
            );

            if ($fieldAnnotation->key && $key === null)
            {
                $key = $property->getName();
            }
        }

        $values = [];

        foreach ($entities as $item)
        {
            $data = (array)$item;

            $value = [];
            foreach ($fields as $field)
            {
                $fieldValue = $data[$field->getName()];

                if ($field->getAnnotation()->nullable)
                {
                    if ($fieldValue === null)
                    {
                        $value[] = "Null";
                    }
                    else
                    {
                        $value[] = $this->getValue($field, $fieldValue);
                    }
                }
                else
                {
                    if ($fieldValue === null)
                    {
                        throw new DatabaseException("Nullable field {$field->getName()} must be annotated nullable");
                    }

                    $value[] = $this->getValue($field, $fieldValue);
                }
            }

            $valueQuery = implode(',', $value);

            $values[] = "($valueQuery)";
        }

        $valuesQuery = implode(',', $values);


        $fieldsNames = array_map(
            callback: static fn(FieldEntity $field) => $field->getName(),
            array: $fields
        );

        $fieldsNamesQuery = implode(',', $fieldsNames);

        $editableFieldsNames = array_map(
            callback: fn(FieldEntity $field) => $this->tempValueStatement($field->getName()),
            array: array_filter(
                array: $fields,
                callback: static fn(FieldEntity $field) => $field->getAnnotation()?->editable === true
            )
        );


        if (count($editableFieldsNames) > 0)
        {
            $editableFieldsNamesQuery = implode(',', $editableFieldsNames);
            $query = $this->insertQuery(
                table: $tableName,
                fields: $fieldsNamesQuery,
                values: $valuesQuery,
                key: $key ?? 'id',
                editableFields: $editableFieldsNamesQuery
            );
        }
        else
        {
            $query = $this->insertQuery(
                table: $tableName,
                fields: $fieldsNamesQuery,
                values: $valuesQuery,
                key: $key ?? 'id',
                editableFields: null
            );
        }

        return $query;
    }

    /**
     * @param FieldEntity $field
     * @param string|int|float|bool $fieldValue
     * @return string
     * @throws DatabaseException
     */
    private function getValue(FieldEntity $field, string|int|float|bool $fieldValue): string
    {
        $typeName = $field->getProperty()->getType()?->getName();

        if ($typeName === 'string')
        {
            if ($field->getAnnotation()->escape)
            {
                return "'{$this->escapeCharacters($fieldValue)}'";
            }

            return "'$fieldValue'";
        }

        if ($typeName === 'int' || $typeName === 'float' || $typeName === 'bool')
        {
            if (is_numeric($fieldValue))
            {
                return (string)$fieldValue;
            }

            if (is_bool($fieldValue))
            {
                return ($fieldValue) ? 'true' : 'false';
            }
        }
        throw new DatabaseException();
    }

    /**
     * @template T
     * @param class-string<T> $class
     * @param array $data
     * @return T
     * @throws DatabaseException
     * @noinspection PhpUndefinedClassInspection
     * @noinspection PhpMissingParamTypeInspection
     * @noinspection PhpUnused
     */
    public function dataToObject(array $data, $class)
    {
        try
        {
            $mapper = new JsonMapper();
            $reflect = new ReflectionClass($class);
            $object = $reflect->newInstanceWithoutConstructor();
            return (object)$mapper->map((object)$data, $object);

        } catch (ReflectionException|JsonMapper_Exception $e)
        {
            throw new DatabaseException($e->getMessage());
        }
    }
}
