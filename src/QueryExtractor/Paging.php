<?php


namespace LaravelModularDashboard\QueryExtractor;


final class Paging
{
    private int $offset;
    private int $pageSize;

    private int $page;

    /**
     * PagingBag constructor.
     * @param int $page
     * @param int $pageSize
     */
    public function __construct(int $page, int $pageSize)
    {

        $this->offset = (($page) * $pageSize);
        $this->pageSize = $pageSize;
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @return int
     * @noinspection PhpUnused
     */
    public function getPage(): int
    {
        return $this->page;
    }


}
