<?php


namespace LaravelModularDashboard\QueryExtractor;

final class HttpQuery
{
    private ?Paging $paging;
    /**
     * @var Sorting[]
     */
    private array $sorting;
    /**
     * @var Filter[]
     */
    private array $filters;

    /**
     * @param Paging|null $paging
     * @param Sorting[] $sorting
     * @param Filter[] $filters
     */
    public function __construct(?Paging $paging, array $sorting, array $filters)
    {
        $this->paging = $paging;
        $this->sorting = $sorting;
        $this->filters = $filters;
    }


    /**
     * @return Paging|null
     */
    public function getPaging(): ?Paging
    {
        return $this->paging;
    }

    /**
     * @return Sorting[]
     */
    public function getSorting(): array
    {
        return $this->sorting;
    }

    /**
     * @return Filter[]
     */
    public function getFilters(): array
    {
        return $this->filters;
    }


    public function removeFilter(string $key): ?Filter
    {
        foreach ($this->filters as $filter)
        {
            if ($filter->getField() === $key)
            {
                $this->filters = array_filter($this->filters, static fn(Filter $filter): bool => $filter->getField() !== $key);
                return $filter;
            }
        }
        return null;
    }
}
