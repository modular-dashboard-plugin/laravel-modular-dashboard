<?php /** @noinspection PhpUnused */


namespace LaravelModularDashboard\QueryExtractor;

use MyCLabs\Enum\Enum;

class ValueType extends Enum
{
    private const STRING = "string";
    private const INT = "int";
    private const FLOAT = "float";
    private const BOOL = "bool";
    private const ARRAY = "array";
    private const DATE = "date";


    public static function STRING(): ValueType
    {
        return new self(self::STRING);
    }

    public static function INT(): ValueType
    {
        return new self(self::INT);
    }

    public static function FLOAT(): ValueType
    {
        return new self(self::FLOAT);
    }

    public static function BOOL(): ValueType
    {
        return new self(self::BOOL);
    }

    public static function ARRAY(): ValueType
    {
        return new self(self::ARRAY);
    }

    public static function DATE(): ValueType
    {
        return new self(self::DATE);
    }
}
