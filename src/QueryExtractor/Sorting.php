<?php


namespace LaravelModularDashboard\QueryExtractor;


use JetBrains\PhpStorm\Pure;

final class Sorting
{
    private string $field;
    private string $direction;

    /**
     * PagingBag constructor.
     * @param string $field
     * @param string $direction
     */
    public function __construct(string $field, string $direction)
    {
        $this->field = $field;
        $this->direction = $direction;
    }


    #[Pure] public static function create(string $sort) : ?Sorting
    {
        $parts = explode('|', $sort);
        if (count($parts) >= 2)
        {
            $dir = strtoupper(trim($parts[1]));
            if ($dir === 'DESC' || $dir === 'ASC')
            {
                return new Sorting(trim($parts[0]), $dir);
            }
        }
        return null;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getDirection(): string
    {
        return $this->direction;
    }


}
