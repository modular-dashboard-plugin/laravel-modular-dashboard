<?php


namespace LaravelModularDashboard\QueryExtractor\Filters;


use LaravelModularDashboard\QueryExtractor\Filter;

final class StringFilter extends Filter
{

    /**
     * @var string
     */
    private string $value;

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }


}
