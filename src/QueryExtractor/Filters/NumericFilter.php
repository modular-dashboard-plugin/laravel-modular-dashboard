<?php


namespace LaravelModularDashboard\QueryExtractor\Filters;


use LaravelModularDashboard\QueryExtractor\Filter;

final class NumericFilter extends Filter
{
    
    private int|bool|float $value;

    /**
     * @return bool|float|int
     */
    public function getValue(): float|bool|int
    {
        return $this->value;
    }

    /**
     * @param float|bool|int $value
     */
    public function setValue(float|bool|int $value): void
    {
        $this->value = $value;
    }



}
