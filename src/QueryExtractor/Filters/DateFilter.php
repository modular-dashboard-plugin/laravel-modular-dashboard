<?php


namespace LaravelModularDashboard\QueryExtractor\Filters;


use DateTime;
use LaravelModularDashboard\QueryExtractor\Filter;

final class DateFilter extends Filter
{



    private DateTime $start;


    private DateTime $end;

    /**
     * @return DateTime
     */
    public function getStart(): DateTime
    {
        return $this->start;
    }

    /**
     * @param DateTime $start
     */
    public function setStart(DateTime $start): void
    {
        $this->start = $start;
    }

    /**
     * @return DateTime
     */
    public function getEnd(): DateTime
    {
        return $this->end;
    }

    /**
     * @param DateTime $end
     */
    public function setEnd(DateTime $end): void
    {
        $this->end = $end;
    }





}
