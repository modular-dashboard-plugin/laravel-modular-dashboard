<?php


namespace LaravelModularDashboard\QueryExtractor\Filters;



use LaravelModularDashboard\QueryExtractor\Filter;

final class ListFilter extends Filter
{

    /**
     * @var string[]
     */
    private array $value;

    /**
     * @return string[]
     */
    public function getValue(): array
    {
        return $this->value;
    }

    /**
     * @param string[] $value
     */
    public function setValue(array $value): void
    {
        $this->value = $value;
    }


}
