<?php /** @noinspection PhpUnused */


namespace LaravelModularDashboard\QueryExtractor;


use MyCLabs\Enum\Enum;

final class FilterType extends Enum
{


    private const EQUAL = "eq";
    private const NOT_EQUAL = "neq";
    private const LIKE = "lk";
    private const START_LIKE = "slk";
    private const GREATER_THEN = "gt";
    private const LESS_THEN = "lt";
    private const GREATER_THEN_OR_EQUAL = "gte";
    private const LESS_THEN_OR_EQUAL = "lte";
    private const IN = "in";
    private const IN_RANG = "inr";


    public static function EQUAL(): FilterType
    {
        return new self(self::EQUAL);
    }

    public static function NOT_EQUAL(): FilterType
    {
        return new self(self::NOT_EQUAL);
    }

    public static function LIKE(): FilterType
    {
        return new self(self::LIKE);
    }

    public static function START_LIKE(): FilterType
    {
        return new self(self::START_LIKE);
    }

    public static function GREATER_THEN(): FilterType
    {
        return new self(self::GREATER_THEN);
    }

    public static function LESS_THEN(): FilterType
    {
        return new self(self::LESS_THEN);
    }

    public static function GREATER_THEN_OR_EQUAL(): FilterType
    {
        return new self(self::GREATER_THEN_OR_EQUAL);
    }

    public static function LESS_THEN_OR_EQUAL(): FilterType
    {
        return new self(self::LESS_THEN_OR_EQUAL);
    }

    public static function IN(): FilterType
    {
        return new self(self::IN);
    }

    public static function IN_RANG(): FilterType
    {
        return new self(self::IN_RANG);
    }


}
