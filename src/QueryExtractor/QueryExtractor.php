<?php


namespace LaravelModularDashboard\QueryExtractor;

final class QueryExtractor
{


    /**
     * @param array $parameters
     * @return HttpQuery
     */
    public function extract(array $parameters): HttpQuery
    {

        if (isset($parameters["sorts"]) && !empty($parameters["sorts"]))
        {
            $sortingString = (string)$parameters["sorts"];

            $sorting = explode(',', $sortingString);

            $sorting = self::mapNotNull($sorting, static fn(string $sort) => Sorting::create($sort));
        }
        else
        {
            $sorting = [];
        }


        if (isset($parameters['filters']) && !empty($parameters['filters']))
        {
            $filtersString = (string)$parameters["filters"];

            $filters = explode(',', $filtersString);

            $filters = self::mapNotNull($filters, static fn(string $filter) => Filter::create($filter));
        }
        else
        {
            $filters = [];
        }


        if (isset($parameters["page"], $parameters["pageSize"]) && is_numeric($parameters["page"]) && is_numeric($parameters["pageSize"]))
        {
            $page = (int)$parameters["page"];
            $pageSize = (int)$parameters["pageSize"];
            $paging = new Paging($page, $pageSize);
        }
        else
        {
            $paging = null;
        }


        return new HttpQuery(paging: $paging, sorting: $sorting, filters: $filters);
    }

    /**
     * @param array $array
     * @param callable $map
     * @return array
     */
    protected static function mapNotNull(array $array, callable $map): array
    {
        return array_filter(
            array_map(
                static function (string $element) use ($map) {
                    return $map($element);
                },
                $array
            ),
            static function ($element) {
                return $element !== null;
            },
        );
    }
}
