<?php


namespace LaravelModularDashboard\QueryExtractor;


use DateTime;
use Exception;
use LaravelModularDashboard\Exceptions\RouterException;
use LaravelModularDashboard\QueryExtractor\Filters\DateFilter;
use LaravelModularDashboard\QueryExtractor\Filters\ListFilter;
use LaravelModularDashboard\QueryExtractor\Filters\NumericFilter;
use LaravelModularDashboard\QueryExtractor\Filters\StringFilter;


abstract class Filter
{

    private string $field;

    private FilterType $filterType;


    private function __construct()
    {
    }


    /**
     * @param string $filterString
     * @return Filter|null
     * @throws RouterException
     */
    public static function create(string $filterString): ?Filter
    {
        $parts = explode('|', $filterString);

        if (count($parts) >= 4)
        {

            $field = trim($parts[0]);
            $type = trim($parts[1]);
            $value = trim($parts[2]);
            $valueType = trim($parts[3]);


            if (!(empty($value) && $value !== '0') && !empty($field) && !is_numeric($field))
            {
                $filterField = $field;

                $filter = match (ValueType::from($valueType)->getValue())
                {
                    ValueType::STRING()->getValue() => self::buildStringFilter($value),
                    ValueType::INT()->getValue() => self::buildNumericFilter((int)$value),
                    ValueType::FLOAT()->getValue() => self::buildNumericFilter((float)$value),
                    ValueType::BOOL()->getValue() => self::buildNumericFilter((bool)$value),
                    ValueType::ARRAY()->getValue() => self::buildListFilter($value),
                    ValueType::DATE()->getValue() => self::buildDateFilter($value),
                    default => null
                };

                if ($filter !== null)
                {
                    $filter->field = $filterField;
                    $filter->filterType = FilterType::from($type);
                }

                return $filter;
            }

            return null;
        }

        return null;
    }


    /**
     * @param string $value
     * @return StringFilter
     * @noinspection PhpUnused
     */
    private static function buildStringFilter(string $value): StringFilter
    {
        $filter = new StringFilter();
        $filter->setValue($value);
        return $filter;
    }


    /**
     * @param int|float|bool $value
     * @return NumericFilter
     * @noinspection PhpUnused
     */
    private static function buildNumericFilter(int|float|bool $value): NumericFilter
    {
        $filter = new NumericFilter();
        $filter->setValue($value);
        return $filter;
    }

    /**
     * @param string $value
     * @return DateFilter|null
     * @throws RouterException
     * @noinspection PhpUnused
     */
    private static function buildDateFilter(string $value): ?DateFilter
    {
        $values = explode('~', $value);
        if (count($values) === 2)
        {
            try
            {
                $filter = new DateFilter();
                $filter->setStart(new DateTime($values[0]));
                $filter->setEnd(new DateTime($values[1]));
                return $filter;
            } catch (Exception $e)
            {
                throw new RouterException($e->getMessage());
            }
        }
        return null;
    }

    /**
     * @param string $value
     * @return ListFilter
     * @noinspection PhpUnused
     */
    private static function buildListFilter(string $value): ListFilter
    {
        $values = explode('~', $value);
        if (!$values)
        {
            $values = [];
        }
        $filter = new ListFilter();
        $filter->setValue($values);
        return $filter;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return FilterType
     */
    public function getFilterType(): FilterType
    {
        return $this->filterType;
    }

}
